# Getting Started with User Profile Management System

checkout this project.

Do `npm install`. this will install all the required dependencies.

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `Dependencies`

We have added @mui, react-router-dom, react-redux, redux and typescript.

@mui - to use the pre built components
react-router-dom - to manage routing
react-redux & redux - for state management
typescript - allows specifying the types of data being passed around within the code, and has the ability to report errors when the types don't match


### To run in local machine

go to src/api/userProfileAPI.ts file line #11. Change the API URL (only if there is a change while running the APIs).