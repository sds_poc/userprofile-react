import React from 'react';
import './App.css';
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Layout from './components/layout';
import ViewUsers from './components/viewUsers';
import AddOrUpdateUser from './components/addOrUpdateUser';
import NoPathMatch from './components/noPathMatch';

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<Layout />}>
            <Route index element={<ViewUsers />} />
            <Route path="add-or-update" element={<AddOrUpdateUser />} />
            <Route path="*" element={<NoPathMatch />} />
          </Route>
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
