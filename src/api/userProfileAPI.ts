import { IUserProfileResponse, IUserProfileResult } from "../model";

export interface IRequestInfo {
    url: string;
    method: "GET" | "POST" | "PUT" | "DELETE";
    body?: BodyInit;
    contentType?: string;
    isMultiPartData?: boolean;
}

const baseURL = "https://localhost:44319/";

const customFetch = <T>(requestInfo: IRequestInfo): Promise<T> => {
    let options: RequestInit = {
        method: requestInfo.method,
        mode: "cors"
    };

    const requestHeaders: HeadersInit = new Headers();

    if (requestInfo.contentType) {
        // requestHeaders.set("content-type", requestInfo.contentType);
        // requestHeaders.set("content", requestInfo.contentType);
    } else {
        requestHeaders.set("content-type", "application/json");
    }

    options.headers = requestHeaders;

    if (requestInfo.body) {
        if (requestInfo.isMultiPartData) {
            options.body = requestInfo.body;
        } else {
            options.body = JSON.stringify(requestInfo.body);
        }
    }

    return new Promise((res, rej) => {
        fetch(requestInfo.url, options)
            .then(response => {
                if (response.headers.get("content-type") && response.headers.get("content-type")!.indexOf("application/json") > -1) {
                    response.json()
                        .then(jsonResponse => {
                            res(jsonResponse);
                        })
                        .catch(err => {
                            rej(err);
                        })
                }
            })
            .catch(err => {
                rej(err);
            })
    });
}

const getUsers = (): Promise<IUserProfileResponse> => {
    return customFetch<IUserProfileResponse>({ method: "GET", url: `${baseURL}api/UserProfile` });
}

const addOrUpdateUser = (userProfile: any): Promise<IUserProfileResult> => {
    return customFetch<IUserProfileResult>({ method: "POST", url: `${baseURL}api/UserProfile`, body: userProfile });
}

const deleteUser = (userId: number): Promise<IUserProfileResult> => {
    return customFetch<IUserProfileResult>({ method: "DELETE", url: `${baseURL}api/UserProfile/?userId=${userId}` });
}

const uploadPhoto = (photo: File, userId: number): Promise<IUserProfileResult> => {
    const formData = new FormData();
    formData.append("file", photo);

    return customFetch<IUserProfileResult>({ method: "POST", url: `${baseURL}api/UserProfile/upload/${userId}`, body: formData, contentType: "multipart/form-data", isMultiPartData: true });
}

export { getUsers, addOrUpdateUser, deleteUser, uploadPhoto };