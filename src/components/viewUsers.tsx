import React, { useEffect, useState } from "react";
import { deleteUser, getUsers } from "../api/userProfileAPI";
import { connect } from "react-redux";
import { IApplicationState, ICustomDialogProps, IUserProfile, IViewUsers } from "../model";
import { compose } from "redux";
import { setSeletedUser, setUpdateSuccess, setUsersList } from "../store/actions";
import { useNavigate } from "react-router-dom";
import Button from '@mui/material/Button';
import CustomDialog from "./dialogWrapper";
import CircularProgress from '@mui/material/CircularProgress';
import UserCard from "./userCard";

const ViewUsers = (props: IViewUsers): JSX.Element => {
    const [errorMessage, setErrorMessage] = useState<string>("");
    const [successMessage, setSuccessMessage] = useState<string>("");
    const [apiCallHappened, setApiCallHappened] = useState<boolean>(false);
    const [isLoading, setIsLoading] = useState<boolean>(false);
    const [openDialog, setOpenDialog] = useState(false);
    const [dialogProps, setDialogProps] = useState<Partial<ICustomDialogProps>>({});

    const navigate = useNavigate();

    useEffect(() => {
        setApiCallHappened(props.users && props.users.length > 0);
    }, []); // eslint-disable-line react-hooks/exhaustive-deps

    useEffect(() => {
        const actions = (
            <>
                <Button onClick={() => { setOpenDialog(false); }}>Okay</Button>
            </>
        );

        if (errorMessage || successMessage) {
            setDialogProps({
                actions: actions,
                content: errorMessage ? errorMessage : successMessage,
                onClose: () => {
                    setOpenDialog(false);
                    setDialogProps({});
                    errorMessage ? setErrorMessage("") : setSuccessMessage("");
                },
                title: errorMessage ? "Error Message" : "Success Message"
            });

            setOpenDialog(true);
        }
    }, [errorMessage, successMessage]);

    useEffect(() => {
        if (props.isUserInfoUpdated) {
            props.setIsUpdated(false);
            setIsLoading(true);
            getUsers()
                .then((response) => {
                    setIsLoading(false);
                    if (response.statusCode === 0) {
                        props.updateUsers(response.users);
                    } else {
                        setErrorMessage(response.statusDescription);
                    }
                    setApiCallHappened(true);
                })
                .catch(() => {
                    setIsLoading(false);
                });
        }
    }, [props.isUserInfoUpdated]) // eslint-disable-line react-hooks/exhaustive-deps

    const onAddOrEditClick = (selectedUser: IUserProfile | null) => {
        props.updateSelectedUser(selectedUser);
        navigate("/add-or-update");
    }

    const onDeleteConfirmation = (_userIdToDelete: number) => {
        setIsLoading(true);
        deleteUser(_userIdToDelete)
            .then((response) => {
                setIsLoading(false);
                if (response.statusCode === 0) {
                    setSuccessMessage(response.statusDescription);
                    props.setIsUpdated(true);
                } else {
                    setErrorMessage(response.statusDescription);
                }
            })
            .catch(() => {
                setIsLoading(false);
            });
    }

    const onDeleteBtnClick = (userId: number) => {
        setOpenDialog(true);
        const dialogActions = (
            <>
                <Button onClick={() => { setOpenDialog(false); }}>No</Button>
                <Button onClick={() => { onDeleteConfirmation(userId); setOpenDialog(false); }} autoFocus>Yes</Button>
            </>
        )
        setDialogProps({
            actions: dialogActions,
            content: "Are you sure want to delete this user?",
            onClose: () => {
                setOpenDialog(false);
                setDialogProps({});
            },
            title: "Confirmation"
        })
    }

    return (
        <React.Fragment>
            {
                isLoading
                    ?
                    <div className="loader">
                        <CircularProgress />
                    </div>
                    :
                    null
            }
            <div className="content-section">
                <Button className="add-user-btn" size="small" variant="contained" onClick={() => { onAddOrEditClick(null); }}>Add User</Button>
                {
                    props.users && props.users.length && apiCallHappened
                        ?
                        <div className="card-container">
                            {props.users.map((_s: IUserProfile, _i: number, _a: IUserProfile[]) => (
                                <UserCard
                                    {..._s}
                                    onEditClick={() => { onAddOrEditClick(_s); }}
                                    onDeleteClick={() => { onDeleteBtnClick(_s.userId); }}
                                    key={_i + 1}
                                />
                            ))}
                        </div>
                        :
                        <div>No Users found, please click on Add User to add a new user.</div>
                }
            </div>
            <CustomDialog
                isOpen={openDialog}
                onClose={() => {
                    if (dialogProps.onClose)
                        dialogProps.onClose();
                }}
                title={dialogProps.title || ""}
                content={dialogProps.content || ""}
                actions={dialogProps.actions || (<></>)}
            />
        </React.Fragment>
    );
};

const stateToProps = ({ userProfile }: IApplicationState) => ({
    users: userProfile?.users,
    isUserInfoUpdated: userProfile?.isUserInfoUpdated
});

const dispatchToProps = (dispatch: any) => ({
    updateUsers: (users: IUserProfile[]) => {
        dispatch(setUsersList(users));
    },
    updateSelectedUser: (selectedUser: IUserProfile | null) => {
        dispatch(setSeletedUser(selectedUser));
    },
    setIsUpdated: (value: boolean) => {
        dispatch(setUpdateSuccess(value));
    }
});

export default compose(connect(stateToProps, dispatchToProps))(ViewUsers);