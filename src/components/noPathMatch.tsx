import React from "react";

const NoPathMatch = () => {
    return <h1>404 Route not found!!</h1>;
};

export default NoPathMatch;