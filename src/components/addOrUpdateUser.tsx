import React, { useEffect, useRef, useState } from "react";
import { connect } from "react-redux";
import { IAddOrUpdateUser, IApplicationState, ICustomDialogProps, IFormStatus, IUserProfile } from "../model";
import { compose } from "redux";
import { setSeletedUser, setUpdateSuccess } from "../store/actions";
import { addOrUpdateUser, uploadPhoto } from "../api/userProfileAPI";
import { useNavigate } from "react-router-dom";
import CustomDialog from "./dialogWrapper";
import Button from '@mui/material/Button';
import CircularProgress from '@mui/material/CircularProgress';
import TextField from '@mui/material/TextField';

const AddOrUpdateUser = (props: IAddOrUpdateUser): JSX.Element => {

    const [userInfo, setUserInfo] = useState<IUserProfile>({ ...props.selectedUser, password: "" } || {});
    const [userInfoBackUp] = useState<IUserProfile>({ ...props.selectedUser } || {});
    const [isPasswordChange, setIsPasswordChange] = useState<boolean>(false);
    const [isLoading, setIsLoading] = useState<boolean>(false);
    const [profilePhoto, setProfilePhoto] = useState<File | null>(null);
    const fileSelectionInputRef = useRef(null);
    const [successMessage, setSuccessMessage] = useState<string>("");
    const [errorMessage, setErrorMessage] = useState<string>("");
    const [openDialog, setOpenDialog] = useState<boolean>(false);
    const [selectedFileSrc, setSelectedFileSrc] = useState<string | null>(null);
    const [dialogProps, setDialogProps] = useState<Partial<ICustomDialogProps>>({});

    const navigate = useNavigate();

    const onChangePasswordClick = (): void => {
        setIsPasswordChange(true);
    }

    const actions: JSX.Element = (<>
        <Button onClick={() => { setOpenDialog(false); }}>Okay</Button>
    </>);

    useEffect(() => {
        if (profilePhoto) {
            setSelectedFileSrc(URL.createObjectURL(profilePhoto));
        } else {
            setSelectedFileSrc(null);
        }
    }, [profilePhoto]);

    useEffect(() => {
        if (errorMessage || successMessage) {
            setDialogProps({
                actions: actions,
                content: errorMessage ? errorMessage : successMessage,
                onClose: () => {
                    setOpenDialog(false);
                    setDialogProps({});
                    errorMessage ? setErrorMessage("") : setSuccessMessage("");
                },
                title: errorMessage ? "Error Message" : "Success Message"
            });

            setOpenDialog(true);
        }
    }, [errorMessage, successMessage]); // eslint-disable-line react-hooks/exhaustive-deps

    const onImageSelection = (): void => {
        if (fileSelectionInputRef && fileSelectionInputRef.current) {
            (fileSelectionInputRef.current as HTMLInputElement).click();
        }
    }

    const onCancelPasswordClick = (): void => {
        setUserInfo({ ...userInfo, password: "" });
        setIsPasswordChange(false);
    }

    const onFileSelectionChange = (selectedPhoto?: File): void => {
        setProfilePhoto(selectedPhoto || null);
    }

    const onSubmit = (): void => {
        let formStatus = isAllFieldsEntered(userInfo.userId ? "UPDATE" : "CREATE");
        if (!formStatus.state) {
            setDialogProps({
                actions: actions,
                content: formStatus.message,
                onClose: () => {
                    setOpenDialog(false);
                    setDialogProps({});
                },
                title: "Validation Message"
            });
            setOpenDialog(true);

            return;
        }

        if (!userInfo.userId
            || (userInfo.userId
                && (userInfo.name !== userInfoBackUp.name
                    || userInfo.emailId !== userInfoBackUp.emailId
                    || userInfo.password))) {
            setIsLoading(true);
            addOrUpdateUser({ ...userInfo })
                .then((response) => {
                    setIsLoading(false);
                    if (response.statusCode === 0) {
                        if (profilePhoto) {
                            uploadProfilePhoto(response.value);
                        } else {
                            setSuccessMessage(response.statusDescription);
                            props.setIsUpdated();
                            onCancel();
                        }
                    } else {
                        setErrorMessage(response.statusDescription);
                    }
                }).catch(() => {
                    setIsLoading(false);
                    setErrorMessage("Error processing your request, please try again.");
                });
        } else if (profilePhoto) {
            uploadProfilePhoto(userInfo.userId);
        } else {
            setDialogProps({
                actions: actions,
                content: "No changes to update",
                onClose: () => {
                    setOpenDialog(false);
                    setDialogProps({});
                },
                title: "Info"
            });
            setOpenDialog(true);
        }
    }

    const isAllFieldsEntered = (mode: "CREATE" | "UPDATE"): IFormStatus => {
        let message: string[] = [];

        if (!userInfo.name)
            message.push("Name");
        if (!userInfo.emailId)
            message.push("Email Id");
        if (!userInfo.bio)
            message.push("Biography");
        if (!userInfo.password && (mode === "CREATE" || (mode === "UPDATE" && isPasswordChange)))
            message.push("Password");

        if (message.length)
            return { state: false, message: `Please enter ${message.join(", ")}.` };
        else
            return isAllFieldsValid();
    }

    const isAllFieldsValid = (): IFormStatus => {
        let result: IFormStatus = { state: true, message: "" };

        var emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;

        if (!emailRegex.test(userInfo.emailId)) {
            result.state = false;
            result.message = "Email Id is invalid";
        }

        return result;
    }

    const uploadProfilePhoto = (userId: number): void => {
        setIsLoading(true);
        uploadPhoto(profilePhoto as File, userId)
            .then((uploadResponse) => {
                setIsLoading(false);
                if (uploadResponse.statusCode === 0) {
                    setSuccessMessage(uploadResponse.statusDescription);
                    props.setIsUpdated();
                    onCancel();
                } else {
                    setErrorMessage(uploadResponse.statusDescription);
                }
            })
            .catch(() => {
                setIsLoading(false);
                setErrorMessage("Error processing your request, please try again.");
            });
    }

    const onReset = (): void => {
        setUserInfo({ ...userInfoBackUp, password: "" });
        setIsPasswordChange(false);
        setProfilePhoto(null);
        if (fileSelectionInputRef.current)
            (fileSelectionInputRef.current as HTMLInputElement).value = "";
    }

    const onCancel = (): void => {
        props.updateSelectedUser();
        navigate("/");
    }

    return (
        <React.Fragment>
            {
                isLoading
                    ?
                    <div className="loader">
                        <CircularProgress />
                    </div>
                    :
                    null
            }
            <div className="form-content-section">
                <div className="form-fields">
                    <TextField
                        id="user-name"
                        label="Name"
                        type="text"
                        color="secondary"
                        value={userInfo.name}
                        onChange={(_event) => { setUserInfo({ ...userInfo, name: _event.target.value }) }}
                    />
                    <TextField
                        id="user-email"
                        label="Email Id"
                        type="email"
                        color="secondary"
                        value={userInfo.emailId}
                        onChange={(_event) => { setUserInfo({ ...userInfo, emailId: _event.target.value }) }}
                    />
                    {
                        userInfo.userId && !isPasswordChange
                            ?
                            null
                            :
                            <TextField
                                id="user-password"
                                label="Password"
                                type="password"
                                color="secondary"
                                value={userInfo.password}
                                onChange={(_event) => { setUserInfo({ ...userInfo, password: _event.target.value }) }}
                            />
                    }
                </div>
                <div className="form-bio-field">
                    <TextField
                        id="user-bio"
                        label="Biography"
                        type="text"
                        color="secondary"
                        multiline
                        value={userInfo.bio}
                        onChange={(_event) => { setUserInfo({ ...userInfo, bio: _event.target.value }) }}
                    />
                </div>
                <div className="image-section">
                    <img className="user-img-preview" alt="user_img_preview" src={selectedFileSrc || userInfo.profileURL || "../images/no-image.jpg"} onClick={() => { onImageSelection(); }} />
                    <input ref={fileSelectionInputRef} accept="image/jpeg, image/jpg, image/png, image/webp" type="file" hidden onChange={(_event) => { onFileSelectionChange(_event.target.files?.[0]) }} />
                    </div>
            </div>
            <div className="footer-btns">
                {
                    userInfo.userId && !isPasswordChange
                        ?
                        <Button size="small" variant="contained" onClick={() => { onChangePasswordClick(); }}>Change Password</Button>
                        :
                        userInfo.userId
                            ?
                            <Button size="small" variant="contained" onClick={() => { onCancelPasswordClick(); }}>Cancel Password update</Button>
                            :
                            null
                }
                <Button size="small" variant="contained" onClick={() => { onSubmit(); }}>Submit</Button>
                <Button size="small" variant="outlined" color="secondary" onClick={() => { onReset(); }}>Reset</Button>
                <Button size="small" variant="outlined" color="secondary" onClick={() => { onCancel(); }}>Cancel</Button>
            </div>
            <CustomDialog
                isOpen={openDialog}
                onClose={() => {
                    if (dialogProps.onClose)
                        dialogProps.onClose();
                }}
                title={dialogProps.title || ""}
                content={dialogProps.content || ""}
                actions={dialogProps.actions || (<></>)}
            />
        </React.Fragment>
    );
};

const stateToProps = ({ userProfile }: IApplicationState) => ({
    selectedUser: userProfile?.selectedUser
});

const dispatchToProps = (dispatch: any) => ({
    updateSelectedUser: () => {
        dispatch(setSeletedUser(null));
    },
    setIsUpdated: () => {
        dispatch(setUpdateSuccess(true));
    }
});

export default compose(connect(stateToProps, dispatchToProps))(AddOrUpdateUser);