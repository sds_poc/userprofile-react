import React from "react";
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import { ICustomDialogProps } from "../model";

const CustomDialog = (props: ICustomDialogProps) => {
    const { isOpen, onClose, title, content, actions } = props;
    return (
        <>
            <Dialog
                open={isOpen}
                onClose={() => { onClose() }}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
            >
                <DialogTitle id="alert-dialog-title">{title}</DialogTitle>
                <DialogContent>
                    <DialogContentText id="alert-dialog-description">{content}</DialogContentText>
                </DialogContent>
                <DialogActions>{actions}</DialogActions>
            </Dialog>
        </>
    )
}

export default CustomDialog;