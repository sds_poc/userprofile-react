import * as React from 'react';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import { IUserProfile } from '../model';

export interface IUserCardProps extends IUserProfile {
    onEditClick: () => void;
    onDeleteClick: () => void;
}

const UserCard = (props: IUserCardProps): JSX.Element => {
    const { profileURL, name, emailId, bio, onEditClick, onDeleteClick } = props;
    return (
        <Card sx={{ minWidth: 250, maxWidth: 250, minHeight: 450, maxHeight: 450 }}>
            <CardMedia
                component="img"
                alt="User image"
                height="200"
                image={profileURL || "../images/no-image.jpg"}
            />
            <div className='user-card-content'>
                <CardContent className='card-content-style'>
                    <Typography gutterBottom variant="h5" component="div">
                        {name}
                    </Typography>
                    <Typography variant="body2" color="text.secondary" style={{ wordBreak: "break-all" }}>
                        <div>{emailId}</div>
                        <div>{bio}</div>
                    </Typography>
                </CardContent>
                <CardActions>
                    <Button size="small" color="info" onClick={() => { onEditClick(); }}>Edit</Button>
                    <Button size="small" color="error" onClick={() => { onDeleteClick(); }}>Delete</Button>
                </CardActions>
            </div>
        </Card>
    );
}

export default UserCard;