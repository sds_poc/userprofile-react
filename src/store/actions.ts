export const setUsersList = (data: any[]) => {
    return {
        type: "UPDATE_USERS_LIST",
        data: data
    }
}

export const setSeletedUser = (data: any) => {
    return {
        type: "SET_SELECTED_USER",
        data: data
    }
}

export const setUpdateSuccess = (data: boolean) => {
    return {
        type: "SET_UPDATE_SUCCESS",
        data: data
    }
}