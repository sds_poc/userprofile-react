import { Reducer } from "redux";
import { IUserProfileState } from "../model";

export const initialUserProfileState: IUserProfileState = {
    users: [],
    selectedUser: null,
    isUserInfoUpdated: true
}

const userProfileReducer: Reducer<IUserProfileState> = (state = initialUserProfileState, action: any) => {
    switch (action.type) {
        case "UPDATE_USERS_LIST": {
            return { ...state, users: action.data as any[] };
        }
        case "SET_SELECTED_USER": {
            return { ...state, selectedUser: action.data as any };
        }
        case "SET_UPDATE_SUCCESS": {
            return { ...state, isUserInfoUpdated: action.data as any };
        }
        default: {
            return state;
        }
    }
}

export default userProfileReducer;