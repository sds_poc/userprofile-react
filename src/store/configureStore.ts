import { createStore } from "redux";
import { IApplicationState } from "../model";
import createRootReducer from "./rootReducer";

const configureStore = (initialState: IApplicationState) => {
    const store = createStore(
        createRootReducer(),
        initialState as any
    )

    return store;
}

export default configureStore;