export interface IApplicationState {
    userProfile: IUserProfileState;
}

export interface IUserProfileState {
    users: any[];
    selectedUser: any | null;
    isUserInfoUpdated: boolean;
}

export interface IFormStatus {
    state: boolean;
    message: string;
}

export interface IUserProfile {
    userId: number;
    name: string;
    emailId: string;
    password: string;
    profileURL: string;
    bio: string;
}

export interface IUserProfileResponse {
    statusCode: number;
    statusDescription: string;
    users: IUserProfile[];
}

export interface IUserProfileResult {
    value: number;
    statusCode: number;
    statusDescription: string;
}

export interface IAddOrUpdateUser {
    selectedUser: IUserProfile;
    setIsUpdated: () => void;
    updateSelectedUser: () => void;
}

export interface ICustomDialogProps {
    isOpen: boolean;
    onClose: () => void;
    title: string | JSX.Element;
    content: string | JSX.Element;
    actions: string | JSX.Element;
}

export interface IViewUsers {
    users: IUserProfile[];
    isUserInfoUpdated: boolean;
    updateUsers: (users: IUserProfile[]) => void;
    updateSelectedUser: (selectedUser: IUserProfile | null) => void;
    setIsUpdated: (value: boolean) => void;
}